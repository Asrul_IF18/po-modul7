public class StudentRecordExample {
	public static void main (String[] args) {
		// membuat 3 object StudentRecord
		StudentRecord annaRecord = new StudentRecord();
		StudentRecord beahRecord = new StudentRecord();
		StudentRecord crisRecord = new StudentRecord();
		// memberi nama siswa
		annaRecord.setName("Anna");
		// melengkapi untuk nama 2 siswa lainnya
		// menampilkan nama siswa "Anna"
		System.out.println(annaRecord.getName() );
		// lengkapi untuk 2 nama lainnya
		// Menampilkan jumlah siswa
		System.out.println("Count=" +StudentRecord.getStudentCount() );
	}
}